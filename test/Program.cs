﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    static class Program  // program klassi ette pane static, siis ei pea eraldi
        // klassi oma extensionite jaoks tegema
    {
        public static TimeSpan Teisenda(this string x)
        {
            return 
                TimeSpan.Parse(
                    string.Join(":",
                            ("0:0:" + x)   // lisab ette mõttetud "0:"-d
                                .Split(':')   // teeb stringi massiiviks
                                .Reverse()    // pöörab massiivi tagurpidi
                                .Take(3)      // võtab kolm esimest (ehk siis viimast)
                                .Reverse()   // pöörab uuesti tagurpidi
                                )
                               );
            

        }


        static void Main(string[] args)
        {
            string isikukood = "35503070211";
            Console.WriteLine(DateTime.TryParse($"{((isikukood[0] - '1') / 2 + 18)}{isikukood.Substring(1, 2)}/{isikukood.Substring(3, 2)}/{isikukood.Substring(5, 2)}", out DateTime dt));
            string[] kellaajad =
            {
                "10.5", // 10 sec
                "2:10.7",  // 2 min 10 sec
                "1:10:20" // tund 10 min 20 sec
            };
			// selle kommentaari ohoo kirjutan ma teisest masinast
            var k = kellaajad
                .Select(x => ("0:0:"+x)   // lisab ette mõttetud "0:"-d
                            .Split(':')   // teeb stringi massiiviks
                            .Reverse()    // pöörab massiivi tagurpidi
                            .Take(3)      // võtab kolm esimest (ehk siis viimast)
                            .Reverse())   // pöörab uuesti tagurpidi
                .Select(x => string.Join(":", x)) // paneb tagasi kokku stringiks
                .Select(x => TimeSpan.Parse(x))   // teisendab TimeSpanniks
                ;
            foreach (var x in k) Console.WriteLine(x);

            Console.WriteLine("\nsiit algab spordipäev\n");

            string filename = @"..\..\spordipäeva protokoll.txt";

            var filesisu = File.ReadAllLines(filename)
                .Skip(1)                // päiserida vahele
                .Select(x => x.Trim())  // ülearused tühikud minema
                .Where(x => x.Length > 0)  // tühjad read välja
                .Select(x => x.Split(','))
                .Select(x => new { Nimi = x[0].Trim(), Distants = x[1].Trim(), Aeg = x[2].Trim().Teisenda()})
                .ToArray()             // et jääks meelde ja ei peaks korduvalt lugema
                ;
            foreach (var x in filesisu) Console.WriteLine(x);


            Console.WriteLine("\nkõige kiirem 100m jooksja\n");
            Console.WriteLine(
                filesisu.Where(x => x.Distants == "100")
                .OrderBy(x => x.Aeg)
                .Take(1)
                .SingleOrDefault()    // kohe kommentaar (vt ekraanil)
                ?.Nimi  ?? "sellist pole"
                );

            Console.WriteLine("\nkõige kiirem jooksja\n");

            Console.WriteLine(
                filesisu
                .OrderByDescending(x => int.Parse(x.Distants) / x.Aeg.TotalSeconds)
                .FirstOrDefault()
                ?.Nimi ?? "keegi ei jooksnudki"
                );

            Console.WriteLine("\nkõige stabiilsem jooksja\n");
            // arvesse lähevad vaid need, kes jooksid mitu distantsi
            Console.WriteLine(
                filesisu
                .ToLookup(x => x.Nimi)
                .Where(x => x.Count() > 1) // kui teha ennem, siis põdra arvutusi pole vaja meil ju teha
                .Select(x => new {
                        Nimi = x.Key,
                        Arv = x.Count(),
                        Vahe = x.Max(y => int.Parse(y.Distants) / y.Aeg.TotalSeconds) - 
                               x.Min(y => int.Parse(y.Distants) / y.Aeg.TotalSeconds) })
                //.Where(x => x.Arv > 1)                
                .OrderBy(x => x.Vahe)
                .FirstOrDefault()
                ?.Nimi ?? "keegi ei jooksnudki mitut distantsi"

                );

        }
    }
}
